import React, { useState } from "react";
import Layout from "../../components/layout";
import StyleModal from "../../components/modalUtills/styleModal";
import css from "@emotion/css";
import { toast } from "react-toastify";

const Attendance = () => {
  const [showModal, setShowModal] = useState(false);
  const [showModal2, setShowModal2] = useState(false);

  return (
    <React.Fragment>
      <Layout>
        <h1>Basic Buttons</h1>
        <div>
          <button
            className=" buttonGreen"
            onClick={() => toast.success("Success Toast!")}
          >
            Save Button
          </button>
          &nbsp;
          <button
            className=" buttonRed"
            onClick={() => toast.error("Error Toast!")}
          >
            Delete Button
          </button>{" "}
          &nbsp;
          <button className=" buttonYellow">Edit Button</button> &nbsp;
          <button className=" buttonBlue">Other Button</button>
        </div>
        <br />
        <div>
          <button className=" buttonGreen" disabled>
            Save Button
          </button>
          &nbsp;
          <button className=" buttonRed" disabled>
            Delete Button
          </button>
          &nbsp;
          <button className=" buttonYellow" disabled>
            Edit Button
          </button>
          &nbsp;
          <button className=" buttonBlue" disabled>
            Other Button
          </button>
        </div>
        <br />
        <h1>Input Fields</h1> // Adjust Height of Input and select to same
        <div>
          <input
            className="basicInput"
            type="text"
            placeholder="Your Input "
          ></input>
          &nbsp; &nbsp; &nbsp;
          <select name="qualificationType" className="basicSelect">
            <option selected>Select Your Option</option>
            <option>Option 1</option>
            <option>Option 2</option>

            <option>Option 3</option>
          </select>
        </div>
        <div>
          <h1>Dynamic Modals</h1>
          <button
            className=" buttonBlue"
            onClick={() => setShowModal(!showModal)}
          >
            Open Modal
          </button>
          &nbsp;
          <button
            className=" buttonBlue"
            onClick={() => setShowModal2(!showModal2)}
          >
            Open Modal Options
          </button>
        </div>
        {showModal && (
          <StyleModal
            headder="Custom Modal"
            handleClose={() => setShowModal(!showModal)}
            buttons={
              <>
                <button className="buttonBlue">Open Modal</button>
              </>
            }
            size={"80%"}
          >
            <table className="content-table">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Views</th>
                  <th>Views2</th>
                  <th>Views3</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Views</th>
                  <th>Views2</th>
                  <th>Views3</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
                <tr>
                  <td>
                    A Long and Winding Tour of the History of UI Frameworks and
                    Tools and the Impact on Design
                  </td>
                  <td>Adam</td>
                  <td>112</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
                <tr>
                  <td>Intro to JavaScript</td>
                  <td>Chris</td>
                  <td>1,280</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
                <tr>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
                <tr>
                  <td>
                    A Long and Winding Tour of the History of UI Frameworks and
                    Tools and the Impact on Design
                  </td>
                  <td>Adam</td>
                  <td>112</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
                <tr>
                  <td>Intro to JavaScript</td>
                  <td>Chris</td>
                  <td>1,280</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                  <td>Adam</td>
                  <td>858</td>
                  <td>Intro to CSS</td>
                  <td>Intro to CSS</td>
                </tr>
              </tbody>
            </table>
          </StyleModal>
        )}
        {showModal2 && (
          <StyleModal
            headder="Custom Modal"
            handleClose={() => setShowModal2(!showModal2)}
            buttons={
              <>
                <button className="buttonRed">Delete Button</button>&nbsp;
                <button className="buttonGreen">Update Button</button>
              </>
            }
          >
            Test Content
          </StyleModal>
        )}
        <div>
          <h1>Basic Table</h1>

          <table className="content-table">
            <thead>
              <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Views</th>
                <th>Views2</th>
                <th>Views3</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Intro to CSS</td>
                <td>Adam</td>
                <td>858</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
              <tr>
                <td>
                  A Long and Winding Tour of the History of UI Frameworks and
                  Tools and the Impact on Design
                </td>
                <td>Adam</td>
                <td>112</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
              <tr>
                <td>Intro to JavaScript</td>
                <td>Chris</td>
                <td>1,280</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
              <tr>
                <td>Intro to CSS</td>
                <td>Adam</td>
                <td>858</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
              <tr>
                <td>
                  A Long and Winding Tour of the History of UI Frameworks and
                  Tools and the Impact on Design
                </td>
                <td>Adam</td>
                <td>112</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
              <tr>
                <td>Intro to JavaScript</td>
                <td>Chris</td>
                <td>1,280</td>
                <td>Intro to CSS</td>
                <td>Intro to CSS</td>
              </tr>
            </tbody>
          </table>
        </div>
      </Layout>
    </React.Fragment>
  );
};

export default Attendance;
