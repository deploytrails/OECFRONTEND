import "../assets/styles/styles.css";
import "react-datepicker/dist/react-datepicker.css";
import "react-toastify/dist/ReactToastify.css";

function App({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
export default App;
